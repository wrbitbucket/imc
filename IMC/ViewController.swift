//
//  ViewController.swift
//  IMC
//
//  Created by Wagner Rodrigues on 05/01/2018.
//  Copyright © 2018 Wagner Rodrigues. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var peso: UITextField!
    @IBOutlet weak var altura: UITextField!
    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var descricao: UILabel!
    @IBOutlet weak var viResultado: UIView!
    
    var resultado: Double = 0
    
    @IBAction func calcularIMC(_ sender: Any) {
        
        if let peso = Double(peso.text!) {
            if let altura = Double(altura.text!) {
                resultado = peso / (altura * altura)
                showResult()
            }
        }
    }
    
    
    func showResult() {
        var result: String = ""
        var image : String = ""
        switch resultado {
        case 0..<16 :
            result = "Magreza"
            image = "abaixo"
        case 16..<18.5:
            result = "Abaixo do peso"
            image = "abaixo"
        case 18.5..<25:
            result = "Peso ideal"
            image = "ideal"
        case 25..<30:
            result = "Sobre peso"
            image = "sobre"
            
        default:
            result = "Obesidade"
            image = "obesidade"
        }
        
        self.descricao.text = "\(Int(resultado)): \(result)"
        self.imagem.image = UIImage(named: image)
        viResultado.isHidden = false
        //view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

}

